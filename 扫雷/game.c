#include"game.h"

//菜单
void menu()
{
	printf("***************************\n");
	printf("******  1. play     *******\n");
	printf("******  0. exit     *******\n");
	printf("***************************\n");
}
//初始棋盘
void InitBoard(char board[ROWplus][COLplus],int row,int col,char set)
{
	for (int i = 0; i < row; i++)
	{
		for (int j = 0; j < col; j++)
		{
			board[i][j] = set;
		}
	}
}
void ShowBoard(char board[ROWplus][COLplus], int row, int col)
{
	printf("------扫雷游戏------\n");
	for (int i = 0; i <=row; i++)
	{
		printf("%d ", i);
	}
	printf("\n");
	for (int i = 1; i <=row; i++)
	{
		printf("%d ", i);
		for (int j = 1; j <= col; j++)
		{
			printf("%c ", board[i][j]);
		}
		printf("\n");
	}
	printf("--------------------\n");
}
//埋雷
void SetBomb(char board[ROWplus][COLplus], int row, int col)
{
	srand((unsigned int)time NULL);
	for (int i = 0; i < Easy_Count; )
	{
		int Rret = rand() % row+1;
		int Cret = rand() % col+1;
		if (board[Rret][Cret] != '1')
		{
			board[Rret][Cret] = '1';
			i++;
		}
		
	}
}
//统计雷的数量
char NumBomb(char board[ROWplus][COLplus], int row, int col)
{

	return board[row - 1][col - 1] + board[row - 1][col] + board[row - 1][col + 1]
		+ board[row][col - 1] + board[row][col + 1]
		+ board[row + 1][col - 1] + board[row + 1][col] + board[row + 1][col + 1]-8*'0';
}
//排查雷
void FindBomb(char originBoard[ROWplus][COLplus], char showBoard[ROWplus][COLplus], int row, int col)
{
	int win = 0;
	//输入所要查找的位置(x,y)
	while (win< row * col - Easy_Count)
	{
		ShowBoard(showBoard, row, col);
		int x, y = 0;
		printf("请输出需要查找的坐标:>");
		scanf("%d %d", &x, &y);
		if (originBoard[x][y] == '1')
		{
			printf("很遗憾，您被炸死了/n");
			break;
		}
		if (x > 0 && x <= row && y > 0 && y <= col)
		{
			if (showBoard[x][y] != '*')
			{
				printf("您已排查过该点，请重新输入\n");
				system("pause");
				system("cls");
				continue;
			}
			int num = NumBomb(originBoard, x, y);
			//如果该位置无雷，展开一片雷区直至下个地区下一片区域的周围8个位置有雷存在为止
			if (num==0)
			{
				Unfold(originBoard, showBoard, row,col,x, y, &win);
			}
			showBoard[x][y] = num + '0';
		}
		else
		{
			printf("输入错误请重新输入");
		}
		system("pause");
		system("cls");

	}
	if (win == row * col - Easy_Count)
	{
		printf("恭喜您 ！已成功过关！\n");
		printf("初始棋盘为：\n");
		ShowBoard(originBoard, row,col);
	}
}
//展开无雷区
void Unfold(char originBoard[ROWplus][COLplus],char showBoard[ROWplus][COLplus],int row,int col,int x,int y,int* win)
{
	//仅访问未访问过的位置，避免无限递归
	if (x>0&&x<=ROW&&y>0&&y<=COL&& showBoard[x][y]=='*')
	{
		//计算该位置雷的个数
		int num = NumBomb(originBoard, x, y);
		//如果该区域周围无雷展开雷区
		if (num==0)
		{
			//标记已经访问过的周围没有雷的位置，防止无限递归
			showBoard[x][y] = ' ';
			//展开雷区，将周围8个位置依次展开直至找到有雷的位置
			Unfold(originBoard, showBoard, row,col,x-1,y-1,win);
			Unfold(originBoard, showBoard, row, col, x-1, y, win);
			Unfold(originBoard, showBoard, row, col, x-1, y+1, win);
			Unfold(originBoard, showBoard, row, col, x, y-1, win);
			Unfold(originBoard, showBoard, row, col, x, y+1, win);
			Unfold(originBoard, showBoard, row, col, x+1, y-1, win);
			Unfold(originBoard, showBoard, row, col, x+1, y, win);
			Unfold(originBoard, showBoard, row, col, x+1, y+1, win);
		}
		//将周围有雷的位置赋值，标记其周围雷的数量
		else
		{
			showBoard[x][y] = num + '0';
		}
		//将标记为空的位置赋值为0
		if (showBoard[x][y] ==' ')
		{
			showBoard[x][y] = '0';
		}
		//获胜标记
		(* win)++;

	}
}

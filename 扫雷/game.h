#define  _CRT_SECURE_NO_WARNINGS 1
// 棋盘大小 
#define COL 9		//列数
#define ROW 9		//行数
#define Easy_Count 1  //简单模式雷的个数
//防止数组内容溢出的加大版棋盘大小
#define COLplus COL+2
#define	ROWplus ROW+2
#include<stdio.h>
#include<stdlib.h>
#include<time.h>

//菜单
void menu();
//初始棋盘
void InitBoard(char board[ROWplus][COLplus], int row, int col,char set);
//显示棋盘
void ShowBoard(char board[ROWplus][COLplus], int row, int col);
//埋雷
void SetBomb(char board[ROWplus][COLplus], int row, int col);
//统计雷的数量
char NumBomb(char board[ROWplus][COLplus], int row,int col);
//排查雷
void FindBomb(char originBoard[ROWplus][COLplus],char showBoard[ROWplus][COLplus] ,int row, int col);
//展开无雷区
void Unfold(char originBoard[ROWplus][COLplus], char showBoard[ROWplus][COLplus], int row, int col, int x, int y, int* win);

#include"game.h"

void PlayGame()
{
	//初始棋盘，无雷处标记为'0' 有雷处标记为'1'的棋盘(便于计算雷的个数)
	char originBoard[ROWplus][COLplus];
	//扫雷游戏中展示的棋牌
	char showBoard[ROWplus][COLplus];
	//初始化初始棋盘
	InitBoard(originBoard, ROWplus, COLplus, '0');
	//初始化展示棋盘
	InitBoard(showBoard, ROWplus, COLplus, '*');
	//在初始棋盘中埋雷
	SetBomb(originBoard, ROW, COL);
	//找雷
	FindBomb(originBoard, showBoard, ROW, COL);
	system("pause");
	system("cls");
}
void ExitGame()
{
	printf("退出游戏，欢迎您下次游玩\n");
	system("pause");
	exit(1);
}
int main()
{
	//游戏
	while (1)
	{
		//菜单
		menu();
		//操作选择
		int choice;
		scanf("%d", &choice);

		switch (choice)
		{
		case 1:
			PlayGame();
			break;
		case 0:
			ExitGame();
			break;
		default:
			printf("输入错误,请重新输入\n");
			system("pause");
			system("cls");
			break;
		}


	}

	return 0;
}


